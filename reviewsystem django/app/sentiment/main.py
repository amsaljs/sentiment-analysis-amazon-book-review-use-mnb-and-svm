import re
import csv
import pandas as pd
import string
from string import digits
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem.porter import PorterStemmer
from collections import Counter
import matplotlib as mpl
import matplotlib.pyplot as plt
from subprocess import check_output
from wordcloud import WordCloud
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import TfidfVectorizer
from pandas import *
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score
from sklearn import metrics
from sklearn.svm import SVC
from sklearn.naive_bayes import MultinomialNB
from sklearn import linear_model
import numpy as np
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory

resource_package = __name__
# --------------------- Loading data train

def preprocessing_train():
    # ------ Loading Data
    pd.set_option('display.max_colwidth', -1)
    train_data = pd.read_csv(r'app/sentiment/train/train.csv', encoding='latin-1', engine='python')

    # Count Sentiment
    neg = len([x for x in train_data['Sentiment'] if x == '__label__1'])
    pos = len([x for x in train_data['Sentiment'] if x == '__label__2'])
    net = len([x for x in train_data['Sentiment'] if x == '__label__0'])
    neg, pos, net

    # LowerCase
    for i in range(len(train_data)):
        lowercase = str.lower(train_data['Review'].iloc[i])
        train_data['Review'].iloc[i] = lowercase

    # Normaliation

    normalization = csv.reader(open('app/sentiment/corpus/normalisasi.csv', 'r'))

    d = {}
    for row in normalization:
        k,v= row
        d[str.lower(k)] = str.lower(v)
    pat = re.compile(r"\b(%s)\b" % "|".join(d))

    for i in range(len(train_data)):
        text = str.lower(train_data['Review'].iloc[i])
        normal = pat.sub(lambda m: d.get(m.group()), text)
        train_data['Review'].iloc[i]=normal
        print('Data train normalization...')

    # Convert Negation
    negation = csv.reader(open('app/sentiment/corpus/convert negation.csv', 'r'))

    d = {}
    for row in negation:
        k, v = row
        d[str.lower(k)] = str.lower(v)

    pat = re.compile(r"\b(%s)\b" % "|".join(d))
    for i in range(len(train_data)):
        convert = str.lower(train_data['Review'].iloc[i])
        convert = pat.sub(lambda m: d.get(m.group()), convert)
        train_data['Review'].iloc[i] = convert
    print('Data train negation word converting...')

    """Remove punctuation from list of tokenized words"""
    new_words = []
    for word in train_data:
        new_word = re.sub(r'[^\w\s]', '', word)
        if new_word != '':
            new_words.append(new_word)

    # Remove Stopwords
    """Remove stop words from list of tokenized words"""
    new_words = []
    for word in train_data:
        if word not in stopwords.words('english'):
            new_words.append(word)

    # Tokenizing
    stop_words = set(stopwords.words('english'))
    tokens = []
    for words in train_data['Review']:
        word_tokens = word_tokenize(words)
        words = []
        for word in (w for w in word_tokens if not w in stop_words):
            words.append(word)
        tokens.append(words)
    print('Showing results of data train tokenization...\n')

    # Stemming

    cleandata = []
    for words in tokens:
        word = []
        for i in range(0, len(words)):
            ps = PorterStemmer()
            review = [ps.stem(words[i])]
            review = ' '.join(review)
            word.append(review)
        cleandata.append(word)
    print('Showing result of data train stemming...')

    i = 0
    for words in cleandata:
        sentences = ' '.join(words)
        train_data['Review'][i] = sentences
        i = i + 1
    print('Showing cleaned data train...')


    return train_data


# def modelEvaluation(predictions):




def train_model_svm():

    train_data = preprocessing_train()
    # Feature-Extraction
    countVect = CountVectorizer(ngram_range=(1, 1))
    data_countVect = countVect.fit_transform(train_data['Review'])

    print("{} sentences and {} unique words create a matrix of the shape {}.".format(
        len(train_data['Review']),
        len(countVect.get_feature_names()),
        data_countVect.toarray().shape
    ))
    # Create the bag of words matrix
    data_countVect_BoW = data_countVect.toarray()

    X_train, X_val, y_train, y_val = train_test_split(train_data['Review'], train_data['Sentiment'],
                                                      test_size=0.2, random_state=2)

    sentiment_list = list(y_train.value_counts().index)

    X_train_countVect = countVect.fit_transform(X_train)

    X_val_countVect = countVect.transform(X_val)

    neg = len([x for x in y_train if x == '__label__1'])
    pos = len([x for x in y_train if x == '__label__2'])
    net = len([x for x in y_train if x == '__label__0'])

    neg = len([x for x in y_val if x == '__label__1'])
    pos = len([x for x in y_val if x == '__label__2'])
    net = len([x for x in y_val if x == '__label__0'])

    tfidf = TfidfVectorizer(min_df=2)  # minimum document frequency of 2\
    X_train_tfidf = tfidf.fit_transform(X_train)
    tfidf_transformer = TfidfTransformer()
    X_train_tfidf = tfidf_transformer.fit_transform(X_train_countVect)

    print("{} sentences and {} unique words create a matrix of the train shape {}.".format(
        len(train_data),
        len(tfidf.get_feature_names()),
        X_train_tfidf.toarray().shape
    ))

    X_val_tfidf = tfidf_transformer.transform(X_val_countVect)
    print("{} sentences and {} unique words create a matrix of the validation shape {}.".format(
        len(train_data),
        len(tfidf.get_feature_names()),
        X_val_tfidf.toarray().shape
    ))


    # SVM
    svc = SVC(kernel='linear', C=1).fit(X_train_tfidf, y_train)
    svc_predictions = svc.predict(X_val_tfidf)

    # Evaluasi
    accuracy1 = accuracy_score(y_val, svc_predictions)*100
    report1 = metrics.classification_report(y_val, svc_predictions)
    matrix1 = metrics.confusion_matrix(y_val, svc_predictions)

    # modelEvaluation(mnb_predictions)

    return svc_predictions, svc, accuracy1, report1, matrix1

def train_model_mnb():

    train_data = preprocessing_train()
    # Feature-Extraction
    countVect = CountVectorizer(ngram_range=(1, 1))
    data_countVect = countVect.fit_transform(train_data['Review'])

    print("{} sentences and {} unique words create a matrix of the shape {}.".format(
        len(train_data['Review']),
        len(countVect.get_feature_names()),
        data_countVect.toarray().shape
    ))
    # Create the bag of words matrix
    data_countVect_BoW = data_countVect.toarray()

    X_train, X_val, y_train, y_val = train_test_split(train_data['Review'], train_data['Sentiment'],
                                                      test_size=0.2, random_state=2)

    sentiment_list = list(y_train.value_counts().index)

    X_train_countVect = countVect.fit_transform(X_train)

    X_val_countVect = countVect.transform(X_val)

    neg = len([x for x in y_train if x == '__label__1'])
    pos = len([x for x in y_train if x == '__label__2'])
    net = len([x for x in y_train if x == '__label__0'])

    neg = len([x for x in y_val if x == '__label__1'])
    pos = len([x for x in y_val if x == '__label__2'])
    net = len([x for x in y_val if x == '__label__0'])

    tfidf = TfidfVectorizer(min_df=2)  # minimum document frequency of 2\
    X_train_tfidf = tfidf.fit_transform(X_train)
    tfidf_transformer = TfidfTransformer()
    X_train_tfidf = tfidf_transformer.fit_transform(X_train_countVect)

    print("{} sentences and {} unique words create a matrix of the train shape {}.".format(
        len(train_data),
        len(tfidf.get_feature_names()),
        X_train_tfidf.toarray().shape
    ))

    X_val_tfidf = tfidf_transformer.transform(X_val_countVect)
    print("{} sentences and {} unique words create a matrix of the validation shape {}.".format(
        len(train_data),
        len(tfidf.get_feature_names()),
        X_val_tfidf.toarray().shape
    ))


    # Naive Bayes
    mnb = MultinomialNB().fit(X_train_tfidf, y_train)
    mnb_predictions = mnb.predict(X_val_tfidf)

    # Evaluasi
    accuracy = accuracy_score(y_val, mnb_predictions)*100
    report = metrics.classification_report(y_val, mnb_predictions)
    matrix = metrics.confusion_matrix(y_val, mnb_predictions)

    # modelEvaluation(mnb_predictions)

    return mnb_predictions, mnb, accuracy, report, matrix


def test_model_mnb(book_data):

    train_data = pd.read_csv(r'app/sentiment/train/train.csv', encoding='latin-1', engine='python')
    X_train_new = train_data['Review']
    y_train_new = train_data['Sentiment']

    X_book = book_data['Review']
    y_book = book_data['Sentiment']

    tfidf = TfidfVectorizer(min_df=2)  # minimum document frequency of 2
    X_train_new_tfidf = tfidf.fit_transform(X_train_new)

    countVect = CountVectorizer(ngram_range=(1, 1))
    X_train_new_countVect = countVect.fit_transform(X_train_new)

    print('Fit and transform the training data to a document-term matrix using TfidfTransformer...')
    tfidf_new_transformer = TfidfTransformer()
    X_train_new_tfidf = tfidf_new_transformer.fit_transform(X_train_new_countVect)

    tfidf_transformer = TfidfTransformer()
    X_book_countVect = countVect.transform(X_book)
    X_book_tfidf = tfidf_transformer.fit_transform(X_book_countVect)

    mnb = MultinomialNB().fit(X_train_new_tfidf, y_train_new)
    mnb_book_predictions = mnb.predict(X_book_tfidf)

    csvfile = "app/sentiment/result/mnb_book_predictions.csv"
    with open(csvfile, "w") as output:
        writer = csv.writer(output)
        writer.writerows(mnb_book_predictions)

    book_neg_mnb = len([x for x in mnb_book_predictions if x == u'__label__1'])
    book_pos_mnb = len([x for x in mnb_book_predictions if x == u'__label__2'])
    book_netral_mnb = len([x for x in mnb_book_predictions if x == u'__label__0'])

    book_BR_mnb = float(book_pos_mnb - book_neg_mnb)
    book_NBR_mnb = (book_BR_mnb / (book_pos_mnb + book_neg_mnb))*100

    return mnb_book_predictions, book_neg_mnb, book_pos_mnb, book_netral_mnb, book_BR_mnb, book_NBR_mnb

def test_model_svm(book_data):

    train_data = pd.read_csv(r'app/sentiment/train/train.csv', encoding='latin-1', engine='python')
    X_train_new = train_data['Review']
    y_train_new = train_data['Sentiment']

    X_book = book_data['Review']
    y_book = book_data['Sentiment']

    tfidf = TfidfVectorizer(min_df=2)  # minimum document frequency of 2
    X_train_new_tfidf = tfidf.fit_transform(X_train_new)

    countVect = CountVectorizer(ngram_range=(1, 1))
    X_train_new_countVect = countVect.fit_transform(X_train_new)

    print('Fit and transform the training data to a document-term matrix using TfidfTransformer...')
    tfidf_new_transformer = TfidfTransformer()
    X_train_new_tfidf = tfidf_new_transformer.fit_transform(X_train_new_countVect)

    tfidf_transformer = TfidfTransformer()
    X_book_countVect = countVect.transform(X_book)
    X_book_tfidf = tfidf_transformer.fit_transform(X_book_countVect)

    svc = SVC(kernel='linear', C=1).fit(X_train_new_tfidf, y_train_new)
    svc_book_predictions = svc.predict(X_book_tfidf)

    csvfile = "app/sentiment/result/svc_book_predictions.csv"
    with open(csvfile, "w") as output:
        writer = csv.writer(output, lineterminator='\n')
        writer.writerows(svc_book_predictions)

    book_neg_svc = len([x for x in svc_book_predictions if x == u'__label__1'])
    book_pos_svc = len([x for x in svc_book_predictions if x == u'__label__2'])
    book_netral_svc = len([x for x in svc_book_predictions if x == u'__label__0'])
    book_neg_svc, book_pos_svc, book_netral_svc

    book_BR_svc = float(book_pos_svc - book_neg_svc)
    book_NBR_svc = (book_BR_svc / (book_pos_svc + book_neg_svc))*100


    return svc_book_predictions, book_neg_svc, book_pos_svc, book_netral_svc, book_BR_svc, book_NBR_svc


def to_lowercase(words):
    """Convert all characters to lowercase from list of tokenized words"""
    for i in range(len(words)):
        lowercase = str.lower(words['Review'].iloc[i])
        words['Review'].iloc[i] = lowercase
    return words

def normalizaton(train_data):
    normalization = csv.reader(open('app/sentiment/corpus/normalisasi.csv', 'r'))
    d = {}
    for row in normalization:
        k, v = row
        d[str.lower(k)] = str.lower(v)
    pat = re.compile(r"\b(%s)\b" % "|".join(d))

    for i in range(len(train_data)):
        text = str.lower(train_data['Review'].iloc[i])
        normal = pat.sub(lambda m: d.get(m.group()), text)
        train_data['Review'].iloc[i] = normal

    return train_data

def negation(book_data):
    negation = csv.reader(open('app/sentiment/corpus/convert negation.csv','r'))

    d = {}
    for row in negation:
        k, v = row
        d[str.lower(k)] = str.lower(v)
        # print d[k]
    pat = re.compile(r"\b(%s)\b" % "|".join(d))

    for i in range(len(book_data)):
        convert = str.lower(book_data['Review'].iloc[i])
        convert = pat.sub(lambda m: d.get(m.group()), convert)
        book_data['Review'].iloc[i] = convert

    return book_data

def remove_punctuation(words):
    """Remove punctuation from list of tokenized words"""
    string.punctuation
    remove = string.punctuation
    for i in range(len(words)):
        sent = words['Review'].iloc[i]
        kd = ' '.join(word.strip(remove) for word in sent.split())
        words['Review'].iloc[i] = kd
    return words

def remove_stopwords(words):
    """Remove stop words from list of tokenized words"""
    cachedStopWords = set(stopwords.words("english"))
    for i in range(len(words)):
        meaningful = words['Review'].iloc[i]
        meaningful = " ".join([word for word in meaningful.split() if word not in cachedStopWords])
        words['Review'].iloc[i] = meaningful
    return words

def tokenizing(train_data):
    factory = StemmerFactory()
    stemmer = factory.create_stemmer()
    for i in range(len(train_data)):
        sent = train_data['Review'].iloc[i]
        output = stemmer.stem(sent)
        train_data['Review'].iloc[i] = output

        train_data = train_data
    return train_data


def stemming(tokens):
    cleandata = []
    for words in tokens:
        word = []
        for i in range(0, len(words)):
            ps = PorterStemmer()
            review = [ps.stem(words[i])]
            review = ' '.join(review)
            word.append(review)
        cleandata.append(word)
    print('Showing result of data train stemming...')

    return cleandata

def show_prep(cleandata):
    i = 0
    for words in cleandata:
        sentences = ' '.join(words)
        cleandata['Review'][i] = sentences
        i = i + 1

    return cleandata

# def convertToDict(words):
#     columns = ['Review Customer', 'Sentiment']
#     DF = pd.DataFrame(columns=columns)
#
# 	for i in range(len(words)):
#         df = pd.DataFrame({'Customer Review':[], 'Sentiment':[]})
# 		obj = {}
# 		obj['Review'] = words.loc[i]['Customer Review']
# 		obj['Sentiment'] = words.loc[i]['Sentiment']
# 		words.append(obj)
#
# 	return words


