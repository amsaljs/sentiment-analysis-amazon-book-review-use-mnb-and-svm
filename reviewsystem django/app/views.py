from django.shortcuts import render
from app.sentiment import main
import pandas as pd
# from django_tables2.tables import Table
import csv
import re
import string

# pd.set_option('display.max_colwidth',-1)
# train_data = pd.read_csv(r'app/sentiment/train/train1.csv', encoding='latin-1', engine='python')


# Create your views here.
def form_index(request):
    return render(request, 'app/index.html')

def sentiment(request):
    pd.set_option('display.max_colwidth', -1)
    book_data = pd.read_csv(r'app/sentiment/test/threewishes.csv', encoding='latin-1', engine='python')

    # ------------------ Lower Case
    lowercase = main.to_lowercase(book_data)
    columns = ['Sentiment', 'Review',]
    DF = pd.DataFrame(columns=columns)

    lower = DF.append(pd.DataFrame(lowercase, columns=columns),
                                               ignore_index=True)

    # ------------ Normalization
    normal = main.normalizaton(book_data)
    normals = DF.append(pd.DataFrame(normal, columns=columns),
                      ignore_index=True)

    negation = main.negation(normal)
    negations = DF.append(pd.DataFrame(negation, columns=columns),
                      ignore_index=True)

    # ------------ Punctuation
    punctuation = main.remove_punctuation(negation)
    punct = DF.append(pd.DataFrame(punctuation, columns=columns),
                        ignore_index=True)

    # ------------ Stopwords
    stopwords = main.remove_stopwords(punctuation)
    stop = DF.append(pd.DataFrame(stopwords, columns=columns),
                        ignore_index=True)

    # tokenizing = main.tokenizing(stopwords)
    # tokens = DF.append(pd.DataFrame(tokenizing, columns="Review"),
    #                  ignore_index=True)

    stemming = main.stemming(stopwords)
    stemmings = DF.append(pd.DataFrame(stopwords, columns=columns),
                       ignore_index=True)

    mnb_predictions, mnb, accuracy,report,matrix = main.train_model_mnb()

    svc_predictions, svc, accuracy1, report1, matrix1 = main.train_model_svm()

    # Test Model
    mnb_book_predictions, book_neg_mnb, book_pos_mnb, book_netral_mnb, book_BR_mnb, book_NBR_mnb = main.test_model_mnb(book_data)
    svc_book_predictions, book_neg_svc, book_pos_svc, book_netral_svc, book_BR_svc, book_NBR_svc = main.test_model_svm(book_data)

    content = {'cal': lower,'pos_mnb':book_pos_mnb, 'neg_mnb':book_neg_mnb,'net_mnb':book_netral_mnb,
               'br_mnb':book_BR_mnb,'nbr_mnb':book_NBR_mnb,'normal': normals,'nmb_predict':mnb_book_predictions,
               'negation':negations,'punctuation': punct, 'stopword':stop,
               'stemming':stemmings, 'mnb':mnb, 'predictions':mnb_predictions,
               'accuracy':accuracy, 'report':report, 'matrix':matrix,
               'svc_predict':svc_predictions,'svc': svc,'acc_svc':accuracy1,'rep_svc':report1,
               'mat_svc':matrix1,
               'predict_svc':svc_book_predictions,'neg_svc':book_neg_svc,'pos_svc':book_pos_svc,'net_svc':book_netral_svc,
               'br_svc':book_BR_svc,'nbr_svc':book_NBR_svc}

    return render(request, 'app/sentiment.html',content)

