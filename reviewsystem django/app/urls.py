# from django.conf.urls import url
from . import views
from django.urls import path


app_name = 'app'
urlpatterns = [
    path('index/', views.form_index),
    path('app/', views.sentiment),
# url(r'^$', views.index, name="index"),
]